CREATE DATABASE  IF NOT EXISTS `BookDTE` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `BookDTE`;
-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: BookDTE
-- ------------------------------------------------------
-- Server version	8.0.37-0ubuntu0.22.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Empresa`
--

DROP TABLE IF EXISTS `Empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Empresa` (
  `EmpresaId` int unsigned NOT NULL AUTO_INCREMENT,
  `EmpresaCod` int unsigned NOT NULL DEFAULT '0',
  `EmpresaRaz` longtext NOT NULL,
  `EmpresaRut` varchar(45) NOT NULL DEFAULT '',
  `EmpresaGir` varchar(45) NOT NULL DEFAULT '',
  `EmpresaDir` varchar(45) NOT NULL DEFAULT '',
  `EmpresaFon` varchar(45) NOT NULL DEFAULT '',
  `EmpresaEma` varchar(45) NOT NULL DEFAULT '',
  `EmpresaRutRep` varchar(45) NOT NULL DEFAULT '',
  `EmpresaNomRep` varchar(45) NOT NULL DEFAULT '',
  `EmpresaCiu` varchar(45) NOT NULL DEFAULT '',
  `EmpresaEliminada` int NOT NULL DEFAULT '0',
  `EmpresaCom` varchar(45) DEFAULT NULL,
  `EmpresaActeco` int unsigned NOT NULL DEFAULT '0',
  `EmpresaOficinaSii` varchar(45) NOT NULL DEFAULT '',
  `EmpresaFechaResolucion` date DEFAULT NULL,
  `EmpresaResolucionNum` int unsigned DEFAULT NULL,
  `SucursalSiiCod` int DEFAULT NULL,
  PRIMARY KEY (`EmpresaId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Empresa`
--

LOCK TABLES `Empresa` WRITE;
/*!40000 ALTER TABLE `Empresa` DISABLE KEYS */;
INSERT INTO `Empresa` VALUES (31,24,'ESTEBAN GABRIEL GUENUL ALMONACID SERVICIOS INFORMATICOS EMPRESA INDIV','76040308-3        ','SERVICIOS INFORMATICOS','URMENETA 305 - OFICINA 513','65-2250340','eguenul@amulen.cl','ESTEBAN GUENUL ALMONACID','13968481-8','PUERTO MONTT',0,'PUERTO MONTT',620200,'PUERTO MONTT','2016-04-25',0,1),(43,45,'OMAR GUENUL V','7485497-4','SERV CONTABLES','URMENETA 305 OFIC 513','652250340','eguenul@yahoo.com','','','PUERTO MONTT',0,'PUERTO MONTT',620200,'','2019-04-30',1,1),(44,46,'EMPRESA PRUEBA','1-9   ','VENTA DE ABARROTES','LOS ALERCES 212','652250340','eguenul@yahoo.com','','','PUERTO MONTT',0,'PUERTO MONTT',620200,'ANTONIO VARAS 500 PTO MONTT','2020-10-28',1,3),(45,47,'ESTEBAN GUENUL','13968481-8','SERV GASTRONOMICOS','URMENETA 305 OFICINA 513','652250340','eguenul@yahoo.com','','','PTO MONTT',0,'PTO MONTT',1,'puerto montt','2020-10-30',0,1),(46,48,'COM LUISA PLACENCIA','76599454-3','COMERCIALIZADORA','URMENETA 305 OFIC 513','975883420','eguenul@yahoo.com','','','PUERTO MONTT',0,'PUERTO MONTT',1,'PUERTO MONTT','2023-02-27',0,1),(47,49,'EDUARDO MADRID','15090794-2','ASESORIAS','URMENETA 305 OFIC 513','975883420','eguenul@yahoo.com','','','PUERTO MONTT',0,'PUERTO MONTT',1,'PUERTO MONTT','2023-03-09',0,1);
/*!40000 ALTER TABLE `Empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TipoDocumentos`
--

DROP TABLE IF EXISTS `TipoDocumentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TipoDocumentos` (
  `TipoDocumentoId` int unsigned NOT NULL AUTO_INCREMENT,
  `TipoDocumentoDes` varchar(45) NOT NULL DEFAULT '',
  `DTE` int unsigned NOT NULL DEFAULT '1',
  `CodigoSii` int unsigned NOT NULL DEFAULT '0',
  `NOTACION` varchar(45) DEFAULT NULL,
  `StockInOut` int DEFAULT '0',
  PRIMARY KEY (`TipoDocumentoId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TipoDocumentos`
--

LOCK TABLES `TipoDocumentos` WRITE;
/*!40000 ALTER TABLE `TipoDocumentos` DISABLE KEYS */;
INSERT INTO `TipoDocumentos` VALUES (1,'FACTURA DE COMPRA ELECTRONICA',1,46,NULL,0),(2,'GUIA DESPACHO ELECTRONICA',1,52,NULL,1),(3,'FACTURA ELECTRONICA',1,33,NULL,1),(4,'NOTA DE CREDITO ELECTRONICA',1,61,NULL,0),(5,'FACTURA EXENTA ELECTRONICA',1,34,NULL,1),(6,'NOTA DEBITO ELECTRONICA',1,56,NULL,1),(7,'FACTURA DE COMPRA',1,45,NULL,1),(8,'FACTURA',1,30,NULL,1),(9,'NOTA DE CREDITO',1,60,NULL,1),(10,'BOLETA ELECTRONICA',1,39,NULL,1),(11,'ORDEN DE COMPRA',1,801,NULL,1),(12,'CARGA STOCK',0,0,'addSTOCK',0),(13,'SALIDA STOCK',0,0,'lowSTOCK',0),(14,'NOTA DE PEDIDO',1,802,NULL,1),(15,'REGISTRO MERMA',0,0,'MERMA',1);
/*!40000 ALTER TABLE `TipoDocumentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Usuario` (
  `UsuarioLogin` varchar(45) NOT NULL DEFAULT '',
  `UsuarioPass` varchar(45) NOT NULL DEFAULT '',
  `UsuarioRut` varchar(45) NOT NULL DEFAULT '',
  `UsuarioEmail` varchar(45) NOT NULL DEFAULT '',
  `UsuarioNom` varchar(45) DEFAULT NULL,
  `UsuarioApell` varchar(45) DEFAULT NULL,
  `ClaveFirma` varchar(45) DEFAULT NULL,
  `NombreArchivo` varchar(45) DEFAULT NULL,
  `BlobCert` longblob,
  PRIMARY KEY (`UsuarioLogin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES ('admin','super','','',NULL,NULL,NULL,NULL,NULL),('eduardomadrid','Edu19041982','15090794-2','eguenul@yahoo.com','EDUARDO','MADRID',NULL,NULL,NULL),('eguenul','amulen1956','13968481-8','eguenul@yahoo.com','ESTEBAN','GUENUL ALMONACID','amulen1956','eguenul.pfx',_binary '0�!0�\�	*�H��\r��\��\�0�\�0�	*�H��\r���0�0�\0*�H��\r\n���0��0\n*�H��\r0��!\�_MG\��\�GqN�)2W\�r�o���{y>XU�\�*/\�ď\�i�\�ә����\�b��CvU\�Q\0q\r�]�\�zimj��\�\�\�W(z��\r�����̧FKc/2r�jFJ�;�1A��\�te#\�H�!�0\r\�\�V�vl\n\\[��=\��c8\�GZΩ\�QaÏ\'k����L\�l��\�ze���<۵��\�O\n��\�\���U����5\�CC\�/��+瀠}!�\�\����W�J_T\�\�W��\�5��Tbۈ���~�\�1��\�\rI\�\�$C\�fm$Q(�V+�?;\�\�߄�(	�e�M9zs\�١e\�\�O9q6���\�\0�L[�C�\r5��\��:\��>\'\\өa�e`\�:B\�lɧ�\�\�3�k�?~��,5����\�_vi2^�\nddE\�c\�\Z\�\�[~�vt&4&#O\�5a�$\��\����ܤ��J�|�wc�F��\��M�Q��\�_N^\��P_\�2�Z�\�XT\�N-*�+\�\�H%\� �d\��\�?ܾ�\�:Q�\�my�\�\�\�\"1\�\�p/\Z�ǚ̊�!��<p\�\Zfc�-�\��\�j�g8\�[�ˬJ٩_�دn��\'��\�R=\�$��\\\\���z� r\�\�<.#[!��1ל\�@�\�լ�=\�.ϻ\�~<V��=�\�*Y5�V�L_\�\��\Z骯\�a\�/\�<x��\\\��R�ݸ��S���6~�k�\�ҁP@F\�\��\��>���\��20���\�p1�\"��\�w�\�g*WP�AL�>t\'ߙ��\���=��22*jテ��\�V3^�޽a.\��\n\��\��\�v\���o\�-r�)B\���yr�\�v\�w��e��\�#,\�0�fP;i��5\�@T��d�͗\�\rP��R\�h�i]\�\��\�\�����恵�2F\�\n\�{\0\�W�\�H�\�4�G#H�W��y\�ة�\��^U�=p�}0>�����0	1]\�1\�<�wnH\�%\�Yd�߲�g(\�\�}p<\��m]\�dvN߳�n\��u�\��u��\���okz��v\"z3�9��-����)\�Ai	\�Q���g�`W[�]5\��\r��~��ן�,�� ��_\��\�[]ld�Q/E\�\�t>=p���L�g����\���:\�\� 3\�\�<\r���d��z�7�>�����S�\'�P3.\��e]�{x\�*��u��\Z����E\�L~\�\��ʢ]j���e\�;��\�8sn���%�\�\��7d[4Q_��[˅ACJ$;��A\�\0\�vl\�\\҅\�\�m\��u\�\rH_J�\�M\���Ѕ��\�n:\�억T9#���+\'a��^����-6fܲ	r/���\�b���O\�9c\�&��C\�\���S+�t��\�1�\�0\r	+�71\00	*�H��\r	1\0\0\00[	*�H��\r	1NL\0{\07\0C\05\05\05\07\09\04\0-\00\0C\00\0A\0-\04\0F\04\06\0-\08\06\04\03\0-\0B\07\08\0F\05\00\09\06\01\07\0C\03\0}0k	+�71^\\\0M\0i\0c\0r\0o\0s\0o\0f\0t\0 \0E\0n\0h\0a\0n\0c\0e\0d\0 \0C\0r\0y\0p\0t\0o\0g\0r\0a\0p\0h\0i\0c\0 \0P\0r\0o\0v\0i\0d\0e\0r\0 \0v\01\0.\000��	*�H��\r���0��\00��	*�H��\r0\n*�H��\r0����\�\�Ѐ�`\�v|:\�1Hi\�\rL\�\0�ww\�3T\��\\��VȭN�<\�\�\��U\�nt�\��>>(�SZ\�و獖�f\�\\�\�\�]}��,{Y/�O�m��=v\�	X�Υ\�,��c\��\�\�\�\�nG\�\'\�R\�\�o�^�\�(\�^\�\Z\�\\m�t���kg�\�)\�\Z��/<W�^\�8��\\�\��+Yj�5˞݇:d)�:\"\��_\"t\�TM��z9�\��dl��T�;4l?����߱*�a\�V�{\�g�&\�T�\��k\Z�\��vX�p����>\�Q\�ȯe���\�ec�-�\�@���\�\�\�L�\�\��H�R:ͭC��g��\�s:\�g?���\0\��~��P��\�c=�\�~�è����`�[}(n�6ܖ\�ȸ\09W�X��-\������+�^\�\�f\���A\��[͞\�z2\��̉�W�\�,�:$)n:�BfB:ܪU\�~�F=IfKE�\�u\�\���z\0K��\�^\�n�\�\�9��\��WN\\9\���\�O4�WK\�1�\�i>+\Z�\�t��rH�%\�Ӣ�\�dv\ZX	\�,ʍcv\�\\z���Ȇl�y]�89w\���`���j\�R��U\"]�c��\n\�P\�\�s^\�NMj}(d@�E\��8pr3�!�\�_W2L\�ܸ�,�Ы\�`hh|�o߄�%z�J{���k\�a���\�����.�څJ���\�\�*\�\��kDAG���\�xX�\n󝮴&<t�Nl>{\�􄏐RW�B\"\nZ�C�\�_ ;f���б�1�\�8w=#�X�����\�\�a&2\Z_�\�/�k\�ݭ77]\�g\�l;\�\�JɊ�\�\���\'�\�B4\��L�yrW\�w\'��0	g�I�\�Ǉ�{.�L*I�ѥh��ӕ>\�M��}\0n\�(��)���\�I1ʁ�\�}+O�\�\�_�3�o�\Zب�8�gH�)ԓ\�\�o�Ժ�\�~)՜S�]H\��w�S\�5�@lH�i�G)��\�d\�r_&\�F�%��7N�54�\� �/iB~�*\�\�z\�j`׳��}b��d3J�ڍ\�v\�kW\"�TN�sx\�\�;�*\��]\�g\�E\�����ݝ\�.�nxgCک�n]��cR\�gY\�ɨ\���čWՍ\'�#\�7\�Kw��\�?\�\�`\����}h\�\�\�\�\�\�\�%:`B�\�(\�/\�F�ڳ��\'��r�(�*	������a�N\�\�&�՛\0\�1O\��\�K�\Z��49\�տ���5�D\�\��p�~\���>zx���y��2_\�<\�y�͓�\0:�\�;�sE�&\r���7BJđ��\�m�\�Xq�3\�/Uݲ\n\��X���{�5\�L#��Q�\�\�(EYЖS\�\0LCک�BVI���;\'V`}�G���;\�E}\��d �\�\0\n�ѝ\�6X�^��Y�P�\���H��t�F�ea\�\�\�\Zj��\�kA\�\�L1_�j\nJa\��,1��y\�[�ɤڔ\�����\�::�\�\�\�w��X�<<�vV\"�\�q��AT��#\�\���i.�4�Y�6w�GE�>��\��Йǻ�l|?���d��S�s0�I\�\�=,�+\�[\���!��#�o,�\�\�F\�\�\���\�R\�N\�\�\�\�N�D�m��Ш�j�vD�\�&9��i\�߼\�ٟÏ\�W�~�ZR+�\�\ra�}ל\�\��\�\�ʱ�љ�h�-c3ɮ\�+n�Ҫ�CE\�b��\�\n�Ok[\�{�\�\�oO<����I�\08�\'�	��@\�E}e;E\�A��X��\�e������6�DL���3\�I\�7K\�o�Qd�\�NZ��N\�zG[y]\�\r�\���\�t�I���򣪞�۫x@@:P�!Zw\�^\�n[�8\��_<*6\�H��\r�\�\�O\'SDYT��?Cr}\\,V]_{\�E-	\��&��(kWa�L���I��4�q�\�7���D`:~�Hz\�- �?t�W�i\�H�K��p\� \�%\�^d�yT\�G_a�ÛPysrmӧh��b�>+o~�Y�d�=��s�}c�uh�(\��JO�\����9y�\�)����%7�a\�\�j\�[Y0�\Z�\� �Ű��Q�X���*\�\��o_ٳ%�U�\�0mp\n\'\�\�5ۂ��\�X�Η�N\�|\�@��M]��P�\�\�b\�\�f�\�hԡsN\�\�6�a��B{��rw�	UH��Ts	��\r�B|:FB��\�O�V��DW�B�od\�;)t\�~;V�NB�~��LqN�\�ƻ�����\�|�.)\�`W�q#�nq�Cn��\�l�GҤ�x�����*F\�\n��L\�� \�7}D�H�\��=�-�A�\�x-o\ZQͻ�\�F����Gq\��Ml\�o�\'\�	���t�L\�ڨ��Q\n�8�7\��\�>/4;�Zbl)b��S0700+\ZUV�l\�}k��)��K\�\�,.z���V[m6h�c�\Z��R\��R�-'),('fmonti','prueba123','1-9','fmonti@coval.cl','FELIPE ANDRES','MONTI HERRERA',NULL,NULL,NULL),('luisaplacencia','Miguel2022','14052871-4','eguenul@yahoo.com','LUISA','PLACENCIA',NULL,NULL,NULL),('oguenul','AMULEN1956','7485497-4','oguenul@amulen.cl','JOSE OMAR ','GUENUL VELASQUEZ',NULL,NULL,NULL),('ROMINA21','ROMINA21','18061211-4','ROMINA21','ROMINA AYESHA','TORO HIDALGO','ROMINA21','ROMINA21.pfx',_binary '0�	0�\�	*�H��\r�����0��0�	*�H��\r���0�0�\0*�H��\r\n���0��0\n*�H��\r0`�XM��P�\��\�k\�ǯ\n+\Z�7�@\����ufl\�\Z�\�j���}\�\�U�\�\"\�\���\�8q\�\��Վe�\�*\�\\lb�l�B\�x��=|`��\�NgHH��\�N\��^m�?\�[�6+0C145m8Ȏ:l�Z��Y��\r./z3�s�J�`��%m!u.�HH?�aC�8���1Y8c\�~�\��[�	��b޽�\n��pz�\�aA��5\�H\�q_��\�\�wu�Z�>�\��\�\�ѱ���\�L1�Wύ�Ed�H��;�\��(m\�ڰ�v\�\�|E\� �p�Z�H�\�j\�N\�\�:-Y�n\�t�\�m \�w\�Mӂ:ȷ�O4%�⍜\�e+�\�\�\��p��~\�٘�\��\�;��^o�7�rZ\�g�@�+�Ny}oWH�i���\"cXW\��\�\�\�B\�6\�#+@\�ݰc� �?DK\n8}�>{\\Z!�\"?\�X69\"qs\�O��\�$V \�X\nT\�\�y%\�@UԿ��Wp�\�\�m\�B�a\r^��]�,N�+U\�p��.�$3\�kF66]���h\�4SK�8yQ\Zhfa�xe\�8�\nhm%\�\�n����h%ۧ\��\"Z�9\�(�\\��_��Q�\�Y�$�5K_c��\�KfI}��i�\�\��6h�Mm/���}\�*okۢ!2�a��cg�c\"����\����\�L	ﮗ[4��f�8\�\�g��$���lJ���m#�\�[\�)@�R{X��	(|闷D�\�V\�+V~��\\n�\�\�^�w|��{P�Uf{�`��ҡz-#����|r\�\�+7\ZXs\�\�r�UcP�Fq\�����r\��=7\�\�9���k@�ߢ!^:\�\n\�VY����\�P�\ZT�c\\\'���\��\Z\�&\�q\��zy��L�r[��?\�W��3�\�D�+��\n�/�\�\�y%B\�{-{\�\�N%\�ic�F\�͏��\�ʒ�\���\"@\��\�\�H��_J\�)7������q]\�\�Ҭ:r��\\��K\�\�]\�\�L�I�\n\Zw	=rЏϕ\��/�*�*�.3��-n3�\�Su��	$u7���x�~Xݦ��\\\Z9�+Et�%��\�\�\��)�(��H�G�u����\�R0�\�9�X\�XP���7�p��\�\��2�X^\�÷�-�;/����O��2�H�ʭh\r;w�~;i\�\�[G��\�\'�_�\�uw��L1+\�8π�PŅ\���WՁm\�\�lX�\�\�*\�LZ���}�0\�;\�1���\�\"oolw��k�{\��V�S\n�[w�fv\�WA��	ʸ�>g���(\��9o\�\�zO~\��ݙ\�\�Xq.>f,$R��M`]�\�\�V`Y��\�R�N�B\�X{s�C��`Ƚ/h�\�\�LW\0U\�\�ѭ�x-r����2�s��\�ٻ\��\�CBu?\�\�kQ\�1�\�0\r	+�71\00	*�H��\r	1\0\0\00[	*�H��\r	1NL\0{\0F\0F\0C\06\04\07\0F\06\0-\0E\0E\0E\04\0-\04\05\03\0F\0-\09\08\08\0F\0-\02\0F\07\04\0F\01\06\0A\0D\02\08\05\0}0k	+�71^\\\0M\0i\0c\0r\0o\0s\0o\0f\0t\0 \0E\0n\0h\0a\0n\0c\0e\0d\0 \0C\0r\0y\0p\0t\0o\0g\0r\0a\0p\0h\0i\0c\0 \0P\0r\0o\0v\0i\0d\0e\0r\0 \0v\01\0.\000��	*�H��\r���0�|\00�u	*�H��\r0\n*�H��\r0����<\�\�Ѐ�HO\Z�\�^�ՅY:{�\�T�2[�N\�+~<�2\\�LM\��~\�ݘ[>2��u�[s\�.��G�u(v�Ņ��f94�k\�\�	A\r\��v\n�s}M(<\�H{�]���9}R���\�O�\�L��z\�\r�x\��q�&.�<����h��+\�Ǹ�\�q?�N\r�\'�x5���a@ S\�n���Bht\�\�\�N�t�`Z`\�n0�M�h\�rJ\�b�ُ0l�M\�<%\�����{/�_Z�Q_�\�8�\�4K\�p�~X;���:�q\�\�14��{\�_�\�E\�(%�O��\�<��\"Iİ^\�\�⁐�\�xJF�\�)\�\��ci\"ma߁\�\n�*=��X�j��`w\�s\�Ó\r\��p�bg\�\ZX�r��u��W5\�\���:�Bߑyh����\�\�#8f�\n\�\\bJ�np�\�m$SZ&\�\�..\�\�LfOl�F�2�aB%E�6�gAk��j�a-�W�:2�/����^E���k1�5��\�f�W\�UĪ\\��3#�\\��}\�N\\։��\�\�!���\�\�_�\�\�\0\�~32\�M��R�$��\�[��m\�\n�sy\�@�No\Z���=*�ь��78\�3\�%8\�_<\��\r|�~��c6�\�$�}\'l�]�IM&��}\��쇘\�y\���]bB\��\�\�|\r꾩�\�T\�\�V>]\�W�oS�zS���\�e+��\"���\�ԩ�Z9SUI߿B\�O�C�_�p�`��Q[�zE�DG͒s�Y�צj\�Ȕ��\�%�n09�<���\�c]́5\�\����v\�f��hO\�⡸�@64\�\�\�\�\�,\�\�n�\�\�i\��ӞV�U.]	\��X}�J��\�Ua�\�\�@�؟Fy|P]z�H�\���=Gʖ	9\�]�\r�\�;�AW�3�5h$\��\�!\�\�\�\�9h\�0;e�\��\�ؽ\�@hn\�\�=�.��ѽ\�\�AD��M�\�$![\�u&�p��Ş�\�\'a\�ތQ\�&�\�k��a�V�\�<�\��\��\�p|�v�\�\�$�\���{`R�^c �	\���v�;ǈy�&\�\���e�\�|E�\�{?�KW�u��5\��<\���&=�\�v\�!\�@�J\�O�:\nSX�\n\�کО�^x�-Y�G��\�1��7{����je\�gJ1�Ԅ@n��Yҗj���i\�Ly\�Q�$\�)7ؑ$�\�#ݲ\�\�.���\�BK_���2D�v0\�\�\\u-��j_\�<w5�T��\��\�\�T\�=\�\�!\�*��*z�\�\�N�	x�\�>�1�O�!\�\�.0j|!�?T�y�&\�\�^\�\��1vG.\�\�\�h�\��s\� ޏ\�g���%Koͤ��\��6\�XSEņ ���f{\�O��\�\�a\n\�d\�oZ�ߵ.y6\��$�ge�E��(�\��@\�t�o�\�Sm�\�U\�=\n\�!ߵv�p!��(����\�\���\��\�\�N�\�\�\�U|\�7y!4U��\�p\�e�����D\�\�9��8=����\�\�J(c�`@kʙ:\�zv\�\�\"\�{v\�)\�ш�\�/��\�{kgـ�{)1k��)��\�\�a�\r�\� �\�[\�p�\��\�.�q\�U�\�\�4�;ν)W�ֻ^�7\�)\����\�\��]iq�O�yT\�h��h.o�E]�\�0�\�K�k���\�MQ�\"\�T\�u\r�\n��\�4\�?֜hk��m���(�b<�\�C�\��*>>�\�m��\�m\�\�]�\�iO@5qI87>�\�*\Z r:N�ϰ\�Wm{$S�n�\��T��7�R}v���T\�DD\�oz�%ϻ���?)g1\�4\�\�d�Z����P!U3.=6�./o\�g\���\�w\�מ�nPFS9\�\�\�9�����������E��;\��=�ï\�\�K�g�*\�\�~B�q\�\��[��4�M��ퟦ��ɪ����{�x\�=�o}E�+\�w�j���n\"���C�b-\�b]V�\�\�\ri\�F�<\��|\�c�$��Q9��\\~t,GTJ1�K�wXM����e�=$j\�L��}�:8a�\'Y)��2�_\�4�#2�\Z}Bq~\�\�Q�\�{�	\�`�\�\�`\Z�\�\�>��.<\�D\�&,�\�\�\'e�`�9?v�zU2k貈O�+�MD�:u%�`D/�Їp@����qt\��\�\���$�����������\�Q�a�Nlẕ�w4Y�:?\�K̶�\�h�\�\�1\��\"���\�bR3�¡r\�T�\��ׄ�CsfnG\�=\�\'JT��=���M!r��V��4�H�Fז0/�k��ml�\0�[?y.XFc9ж\���\Z\�\�s\��1\�O\�Z[?2P\�!\�x\'���\�\�z>�Lh\�--�)4dt4%�\�\�|\�(\"�\� H0700+\Z�5�D\r ]���\�\'S���ړ/[z8�x;��Y\�Ng��m'),('userprueba','test123','1-9','eguenul@yahoo.com','USUARIO','PRUEBA',NULL,NULL,NULL);
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-17  0:17:28
