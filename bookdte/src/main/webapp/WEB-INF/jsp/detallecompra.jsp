<%@page import="com.egga.bookdte.models.DetalleCompra"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.egga.bookdte.include.Funciones"%>

<table class="table table-striped">
<tr>
    <th>RUT RECEPTOR</th>
    <th>RAZON SOCIAL</th>
    <th>DOCUMENTO</th>    
    <th>FOLIO</th>
    <th>FECHA</th>
    <th>MONTO EXENTO</th>
    <th>MONTO NETO</th>
    <th>IVA RECUPERABLE</th>
    <th>IVA NO RECUPERABLE</th>
    <th>IVA DE USO COMUN</th>
    <th>TOTAL BRUTO</th>
   <th>ACCION</th>
</tr>


<%
   Funciones objFunciones = new Funciones(); 
    
   List<DetalleCompra> compras = (List<DetalleCompra>)request.getSession(true).getAttribute("arraycompras");
   for( DetalleCompra compra : compras ){
    
%>    
<tr>
    <td><% out.print(compra. getRUT_Proveedor()); %></td>
    <td><% out.print(compra.getRazon_Social()); %></td>
    <td><% out.print(objFunciones.getNombreDoc(compra.getTipo_Doc())); %></td>    
    <td><% out.print(compra.getFolio()); %></td>
    <td><% out.print(compra.getFecha_Docto()); %></td>
    <td><% out.print(compra.getMonto_Exento()); %></td>
    <td><% out.print(compra.getMonto_Neto()); %></td>
    <td><% out.print(compra.getMonto_IVA_Recuperable()); %></td>
     <td><% out.print(compra.getMonto_Iva_No_Recuperable()); %></td>
     <td><% out.print(compra.getIVA_uso_Comun()); %></td>
   <td><% out.print(compra.getMonto_Total()); %></td>
    <td><button type="button" name="btnLimpiar" onClick="window.location.href='movimiento';" class="btn btn-primary btn-sm">
        <span class="glyphicon glyphicon-file"</span>Nuevo 
        </button>        
    </td>
</tr>
  
  <% } %>
</table>

    
