<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <!-- Se eliminó el contenido vacío -->
  </div>

  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-cog"></span> MANTENCION<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href=empresa>EMPRESAS</a></li>
          <li><a href="usuario">USUARIOS</a></li>
          <li><a href="adminCert">CARGA CERTIFICADO DIGITAL</a></li>
          <li><a href="setFirmaPass">CLAVE FIRMA ELECTRONICA</a></li>
          <li><a href="setpass">CAMBIO PASSWORD ADMINISTRADOR</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-tasks"></span> PROCESOS <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="get-ventas">GENERAR LIBRO DE VENTA</a></li>
          <li><a href="get-compras">GENERAR LIBRO DE COMPRA </a></li>
          
        </ul>
      </li>
          </ul>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <span style="color:white" class="navbar-text">
          RAZON SOCIAL: <% out.print(request.getSession().getAttribute("EmpresaRaz")); %>
        </span>
      </li>

      <li>
        <span style="color:white" class="navbar-text">
          RUT: <% out.print(request.getSession().getAttribute("EmpresaRut")); %>
        </span>
      </li>

      <li><a href="#">
        <span class="glyphicon glyphicon-user"></span>
        <% out.print(request.getSession().getAttribute("login")); %>
      </a></li>

      <li>
        <a href="logout.jsp">
          <span class="glyphicon glyphicon-log-out"></span> SALIR
        </a>
      </li>
    </ul>
  </div>
</nav>
