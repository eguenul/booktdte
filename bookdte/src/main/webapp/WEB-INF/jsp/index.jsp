

<%@page import="com.egga.bookdte.empresa.Empresa"%>
<%
 


Empresa objEmpresa = (Empresa) request.getSession().getAttribute("Empresa");


%>
 <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>    
    
    
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> <!?Con esto garantizamos que se vea bien en dispositivos móviles?> 
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
  <link rel="stylesheet" type="text/css" href="resources/css/estilo.css">
<title>BOOKDTE</title>
    </head>
    <body>
<%@include file="include/navview.jsp" %>
        <div align="center">
            <table>
                <tr>
                    <th colspan="2">
                        EMPRESA SELECCIONADA
                    </th>
                </tr>
                <tr>
                    <td>
                        RAZON SOCIAL
                    </td>
                    <td>
                        <%= request.getSession().getAttribute("EmpresaRaz") %>
                     </td>               
                </tr>
                <tr>
                    <td>
                        RUT
                    </td>
                    <td>
                    <%=  request.getSession().getAttribute("EmpresaRut") %>
                    </td>               
                </tr>
                
            </table>
            
            
        </div>
                 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
