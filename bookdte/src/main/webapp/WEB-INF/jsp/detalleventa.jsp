<%@page import="com.egga.bookdte.models.DetalleVenta"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.egga.bookdte.include.Funciones"%>

<table class="table table-striped">
<tr>
    <th>RUT RECEPTOR</th>
    <th>RAZON SOCIAL</th>
    
    <th>DOCUMENTO</th>    
    <th>FOLIO</th>
    <th>FECHA</th>
    <th>MONTO EXENTO</th>
    <th>MONTO NETO</th>
    <th>IVA</th>
    <th>TOTAL BRUTO</th>
   <th>ACCION</th>
</tr>


<%
   Funciones objFunciones = new Funciones(); 
    
   List<DetalleVenta> ventas = (List<DetalleVenta>)request.getSession(true).getAttribute("arrayventas");
   for( DetalleVenta venta : ventas ){
    
%>    
<tr>
    <td><% out.print(venta.getRUT_cliente()); %></td>
    <td><% out.print(venta.getRazon_Social()); %></td>
    
    <td><% out.print(objFunciones.getNombreDoc(venta.getTipo_Doc())); %></td>    
    <td><% out.print(venta.getFolio()); %></td>
    <td><% out.print(venta.getFecha_Docto()); %></td>
    <td><% out.print(venta.getMonto_Exento()); %></td>
    <td><% out.print(venta.getMonto_Neto()); %></td>
    <td><% out.print(venta.getMonto_IVA()); %></td>
    <td><% out.print(venta.getMonto_Total()); %></td>
    <td><button type="button" name="btnLimpiar" onClick="window.location.href='movimiento';" class="btn btn-primary btn-sm">
        <span class="glyphicon glyphicon-file"</span>Nuevo 
        </button>        
    </td>
</tr>
  
  <% } %>
</table>

    