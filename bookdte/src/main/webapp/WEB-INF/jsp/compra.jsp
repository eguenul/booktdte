<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> <!?Con esto garantizamos que se vea bien en dispositivos m�viles?> 
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css" href="resources/css/estilo.css" media="screen" />
<script src="resources/scripts/ajax.js"></script>        
<title>LIBRO DE COMPRAS</title>
</head>
<body>
    <%@include file="include/navview.jsp" %>
    <div align="center">
    <h1>LIBRO DE COMPRAS</h1>
    </div>
    <form>
      
        <table class="table table-striped">    
            <tr>
                
                
                <td>
       
                    MES
                </td>    
                <td>
        <select id="mes" name="mes">
            <option value="01">Enero</option>
            <option value="02">Febrero</option>
            <option value="03">Marzo</option>
            <option value="04">Abril</option>
            <option value="05">Mayo</option>
            <option value="06">Junio</option>
            <option value="07">Julio</option>
            <option value="08">Agosto</option>
            <option value="09">Septiembre</option>
            <option value="10">Octubre</option>
            <option value="11">Noviembre</option>
            <option value="12">Diciembre</option>
        </select>
                </td>
                <td>
                    A&Ntilde;O
            </td>
                
                <td>
        <input type="text" id="anio" name="anio" pattern="\d{4}" required>
                </td>
                <td> <button type="button" onclick="cargarAjax('get-compras','mes='+document.getElementById('mes').value+'&anio='+document.getElementById('anio').value ,'contenido');">Obtener Datos</button></td>
            
            </tr>
                <!-- comment -->
        </table>
        
       
    </form>
    <div id="contenido"></div>
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>    
</body>
</html>

