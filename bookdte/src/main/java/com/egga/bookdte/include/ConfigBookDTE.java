/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.bookdte.include;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */





import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ConfigBookDTE {
    private final String serveraddress;
    private final String username;
    private final String userpass;
    private final String databasename;
    private final String pathreports;
    private final String pathdownload;
    private final String environment;
    
    public ConfigBookDTE() throws ParserConfigurationException, SAXException, IOException{
        Properties prop = new Properties();
        try (InputStream in = getClass().getResourceAsStream("/bookdte.properties")) {
            prop.load(in);
       
          this.serveraddress = prop.getProperty("server-address");
          this.username = prop.getProperty("user-name");
          this.userpass = prop.getProperty("user-pass");
          this.databasename = prop.getProperty("database-name");
          this.pathreports = prop.getProperty("path-reports");
          this.pathdownload = prop.getProperty("path-download");
          this.environment = prop.getProperty("environment-url");
        }
    }
    
    
    
    public String getUserpass() {
        return userpass;
    }

    public String getServeraddress() {
        return serveraddress;
    }

    public String getUsername() {
        return username;
    }
    
    
  

    public String getDatabasename() {
        return databasename;
    }

    public String getPathreports() {
        return pathreports;
    }

    /**
     * @return the pathdownload
     */
    public String getPathdownload() {
        return pathdownload;
    }

    public String getEnvironment() {
        return environment;
    }
    
    

    
}