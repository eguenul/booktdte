package com.egga.bookdte.service;

import com.egga.bookdte.json.ICVCompraJSON;
import com.google.gson.Gson;
import com.egga.bookdte.models.DetalleCompra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import org.xml.sax.SAXException;

@Service
public class GetComprasService {

    @Autowired
    private ICVCompra objICVCompra;

    public String getComprasJSON(String login, String clavecert, String rutempresa, String mes_periodo, String year_periodo, String serverauth) throws IOException, SAXException, Exception {
      System.out.print("prueba servicios");
        String periodo = year_periodo + mes_periodo;

        // Realizo la petición HTTP para obtener las compras
        String stringCSV = objICVCompra.obtieneCompras(login, clavecert, rutempresa, periodo, serverauth);
        System.out.print(stringCSV);
        // Procedo a formatear el resultado CSV
        ArrayList<DetalleCompra> arraydetallecompra = objICVCompra.formatCSV(stringCSV);

        // Convertir a JSON
        ICVCompraJSON objJSON = new ICVCompraJSON();
        objJSON.setDetalledocumento(arraydetallecompra);
        Gson gson = new Gson();
        String stringJSON = gson.toJson(objJSON);

        System.out.println("\n" + stringJSON);
        return stringJSON;
    }
}