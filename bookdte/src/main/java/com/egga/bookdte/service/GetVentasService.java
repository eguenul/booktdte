/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.bookdte.service;

import com.egga.bookdte.json.ICVVentaJSON;
import com.egga.bookdte.models.DetalleVenta;
import com.google.gson.Gson;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
@Service
public class GetVentasService {
    
     @Autowired
    private ICVVenta objICVVenta;
    public String getVentasJSON( String login,String clavecert, String rutempresa, String mes_periodo, String year_periodo, String serverauth) throws ParserConfigurationException, SAXException, Exception{
       
          
          
                  String periodo = year_periodo+mes_periodo;        
                 /* realizo la peticion HTTP PARA OBTENER LAS VENTAS */
                 String stringCSV =  objICVVenta.obtieneVentas(login, clavecert, rutempresa, periodo,serverauth);
                 objICVVenta.formatCSV(stringCSV);
                 /* PROCEDO A FORMATEAR EL RESULTADO CSV */
                 
                 ArrayList<DetalleVenta>  arraydetalleventa = objICVVenta.formatCSV(stringCSV);
                 
                 
                 
                 
                 
                 
                 
                 ICVVentaJSON  objJSON = new ICVVentaJSON();
                 
                 
             
                objJSON.setDetalledocumento(arraydetalleventa);
                final Gson gson = new Gson();
	        final String stringJSON = gson.toJson(objJSON);  
                 
                  
         
        System.out.print("\n"+stringJSON);
        return stringJSON;
    }
    
    
}
