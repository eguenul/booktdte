/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.bookdte.controller;

import com.egga.bookdte.include.ConfigBookDTE;
import com.egga.bookdte.json.ICVVentaJSON;
import com.egga.bookdte.models.DetalleVenta;
import com.egga.bookdte.service.GetVentasService;
import com.google.gson.Gson;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */

@WebServlet("/get-ventas")
public class GetVentasServlet extends HttpServlet {
    
    private GetVentasService getVentasService;

    
    @Override
    public void init() throws ServletException {
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        getVentasService = context.getBean(GetVentasService.class);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      if (request.getSession().getAttribute("loginauth") == null) {
            response.sendRedirect("login");
            return;
        }
         request.getRequestDispatcher("/WEB-INF/jsp/venta.jsp").forward(request, response);

        
        
   
    } 
    
   
  @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
       try {
            
            System.out.print("extrayendo");
            String login = (String) request.getSession().getAttribute("login");
            String clavecert = (String) request.getSession().getAttribute("clavefirma");
            String rutempresa = (String) request.getSession().getAttribute("EmpresaRut");
            String mes_periodo = request.getParameter("mes");
            String year_periodo = request.getParameter("anio");
            
            ConfigBookDTE objConfig = new ConfigBookDTE();
            
            String serverauth = objConfig.getEnvironment();
      
            try {
                
                
                String jsonResponse = getVentasService.getVentasJSON(login, clavecert, rutempresa, mes_periodo, year_periodo, serverauth);
                System.out.print(jsonResponse);
                Gson gson = new Gson();
                
                InputStream isjson = new ByteArrayInputStream(jsonResponse.getBytes("ISO-8859-1"));
                BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
                
                ICVVentaJSON obJSON = gson.fromJson(br1, ICVVentaJSON.class);
                ArrayList<DetalleVenta> arrayventas = obJSON.getDetalledocumento();
                request.getSession().setAttribute("arrayventas", arrayventas);
                  request.getRequestDispatcher("/WEB-INF/jsp/detalleventa.jsp").forward(request, response);

                
            } catch (Exception e) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error processing request");
            }
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(GetVentasServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
   
    } 
    
}
