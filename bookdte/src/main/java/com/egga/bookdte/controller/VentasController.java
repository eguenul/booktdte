/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.bookdte.controller;

import com.egga.bookdte.service.GetVentasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author esteban
 */

@RestController
public class VentasController {
     @Autowired
    private GetVentasService ventasService;
      @GetMapping("/ventas")
    public String obtenerComprasJSON(
            @RequestParam String login,
            @RequestParam String clavecert,
            @RequestParam String rutempresa,
            @RequestParam String mes_periodo,
            @RequestParam String year_periodo,
            @RequestParam String serverauth) {

        try {
            return ventasService.getVentasJSON(login, clavecert, rutempresa, mes_periodo, year_periodo, serverauth);
        } catch (Exception e) { // Manejo básico de errores, puedes personalizar según tus necesidades
            // Manejo básico de errores, puedes personalizar según tus necesidades
            return "Error al obtener compras: " + e.getMessage(); // Retorna un mensaje de error en caso de excepción
        }
    }
    
}
