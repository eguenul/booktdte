package com.egga.bookdte.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/index")

public class IndexServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              
    

        if (request.getSession().getAttribute("loginauth") == null) {
            response.sendRedirect("login");
            return;
        }

        if (request.getSession().getAttribute("empresaid") == null) {
            response.sendRedirect("listarempresa");
            return;
        }

        getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
    }
}
