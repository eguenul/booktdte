/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.bookdte.controller;


import com.egga.bookdte.include.ConfigBookDTE;
import com.egga.bookdte.json.ICVCompraJSON;
import com.egga.bookdte.models.DetalleCompra;
import com.egga.bookdte.service.GetComprasService;
import com.google.gson.Gson;
import org.springframework.web.context.support.WebApplicationContextUtils;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;
import org.xml.sax.SAXException;

@WebServlet("/get-compras")
public class GetComprasServlet extends HttpServlet {

    private GetComprasService getComprasService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        getComprasService = context.getBean(GetComprasService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            
            System.out.print("extrayendo");
            String login = (String) request.getSession().getAttribute("login");
            String clavecert = (String) request.getSession().getAttribute("clavefirma");
            String rutempresa = (String) request.getSession().getAttribute("EmpresaRut");
            String mes_periodo = request.getParameter("mes");
            String year_periodo = request.getParameter("anio");
            
            ConfigBookDTE objConfig = new ConfigBookDTE();
            
            String serverauth = objConfig.getEnvironment();
      
            try {
                
                
                String jsonResponse = getComprasService.getComprasJSON(login, clavecert, rutempresa, mes_periodo, year_periodo, serverauth);
                System.out.print(jsonResponse);
                Gson gson = new Gson();
                
                InputStream isjson = new ByteArrayInputStream(jsonResponse.getBytes("ISO-8859-1"));
                BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
                
                ICVCompraJSON obJSON = gson.fromJson(br1, ICVCompraJSON.class);
                ArrayList<DetalleCompra> arraycompras = obJSON.getDetalledocumento();
                request.getSession().setAttribute("arraycompras", arraycompras);
                    request.getRequestDispatcher("/WEB-INF/jsp/detallecompra.jsp").forward(request, response);

                
            } catch (Exception e) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error processing request");
            }
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(GetComprasServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

 @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         request.getRequestDispatcher("/WEB-INF/jsp/compra.jsp").forward(request, response);



    }



}
