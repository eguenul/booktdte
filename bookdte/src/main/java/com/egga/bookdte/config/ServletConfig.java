package com.egga.bookdte.config;


import com.egga.bookdte.controller.GetComprasServlet;
import com.egga.bookdte.controller.GetVentasServlet;
import com.egga.bookdte.controller.IndexServlet;
import com.egga.bookdte.empresa.EmpresaServlet;
import com.egga.bookdte.empresa.EmpresaServlet2;
import com.egga.bookdte.empresa.selEmpresaServlet;
import com.egga.bookdte.login.LoginServlet;
import com.egga.bookdte.usuario.adminCert;
import com.egga.bookdte.usuario.UsuarioServlet;

import com.egga.bookdte.usuario.setFirmaPass;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletConfig {
@Bean
public ServletRegistrationBean<GetComprasServlet> getcomprasServlet(){
   ServletRegistrationBean<GetComprasServlet> servletRegistrationBean = new ServletRegistrationBean<>(new GetComprasServlet(), "/get-compras"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
} 

@Bean
public ServletRegistrationBean<GetVentasServlet> getventasServlet(){
   ServletRegistrationBean<GetVentasServlet> servletRegistrationBean = new ServletRegistrationBean<>(new GetVentasServlet(), "/get-ventas"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    



@Bean
public ServletRegistrationBean<LoginServlet> loginServlet(){
   ServletRegistrationBean<LoginServlet> servletRegistrationBean = new ServletRegistrationBean<>(new LoginServlet(), "/login"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    


@Bean
public ServletRegistrationBean<IndexServlet> IndexServlet(){
   ServletRegistrationBean<IndexServlet> servletRegistrationBean = new ServletRegistrationBean<>(new IndexServlet(), "/index"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    


@Bean
public ServletRegistrationBean<selEmpresaServlet> selEmpresaServlet(){
   ServletRegistrationBean<selEmpresaServlet> servletRegistrationBean = new ServletRegistrationBean<>(new selEmpresaServlet(), "/listarempresa"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    




@Bean
public ServletRegistrationBean<EmpresaServlet2> EmpresaServlet2(){
   ServletRegistrationBean<EmpresaServlet2> servletRegistrationBean = new ServletRegistrationBean<>(new EmpresaServlet2(), "/seleccion"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    


@Bean
public ServletRegistrationBean<EmpresaServlet> EmpresaServlet(){
   ServletRegistrationBean<EmpresaServlet> servletRegistrationBean = new ServletRegistrationBean<>(new EmpresaServlet(), "/empresa"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}   


@Bean
public ServletRegistrationBean<adminCert> adminCert(){
   ServletRegistrationBean<adminCert> servletRegistrationBean = new ServletRegistrationBean<>(new adminCert(), "/adminCert"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    

/*

@Bean
public ServletRegistrationBean<uploadCert> uploadCert(){
   ServletRegistrationBean<uploadCert> servletRegistrationBean = new ServletRegistrationBean<>(new uploadCert(), "/uploadCert"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    
*/


@Bean
public ServletRegistrationBean<UsuarioServlet> UsuarioServlet(){
   ServletRegistrationBean<UsuarioServlet> servletRegistrationBean = new ServletRegistrationBean<>(new UsuarioServlet(), "/usuario"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    




@Bean
public ServletRegistrationBean<setFirmaPass> setFirmaPass(){
   ServletRegistrationBean<setFirmaPass> servletRegistrationBean = new ServletRegistrationBean<>(new setFirmaPass(), "/setFirmaPass"); 
   servletRegistrationBean.setLoadOnStartup(-1);
   return servletRegistrationBean;
}    




    
}
