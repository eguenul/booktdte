package com.egga.bookdte.usuario;

import com.egga.bookdte.include.Funciones;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.xml.sax.SAXException;

@Controller
public class uploadCert {

    private static final Logger LOGGER = Logger.getLogger(uploadCert.class.getName());

    @PostMapping("/uploadCert")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   HttpSession session,
                                   RedirectAttributes redirectAttributes) {
        byte[] certificateBytes;
        String login = (String) session.getAttribute("login");

        try {
            // Leer los bytes del archivo subido
            certificateBytes = readBytesFromFile(file);

            // Guardar el certificado utilizando la función `addCertificado`
            Funciones objFunciones = new Funciones();
            objFunciones.addCertificado(login, certificateBytes);

            // Archivo subido exitosamente
            LOGGER.info("CERTIFICADO CORRECTAMENTE CARGADO");
            redirectAttributes.addFlashAttribute("message", "Certificado cargado exitosamente");

        } catch (IOException | ClassNotFoundException | SQLException | ParserConfigurationException | SAXException ex) {
            LOGGER.log(Level.SEVERE, "File Upload Failed due to: " + ex.getMessage(), ex);
            redirectAttributes.addFlashAttribute("message", "File Upload Failed due to " + ex.getMessage());
            return "redirect:/error";
        }

        return "redirect:/cafok";
    }

    private byte[] readBytesFromFile(MultipartFile file) throws IOException {
        try (InputStream inputStream = file.getInputStream();
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

            byte[] buffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
            }

            return byteArrayOutputStream.toByteArray();
        }
    }
}
