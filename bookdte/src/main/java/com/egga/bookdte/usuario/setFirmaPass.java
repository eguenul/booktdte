/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.bookdte.usuario;

import com.egga.bookdte.include.Funciones;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.Enumeration;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */

@WebServlet(urlPatterns = "/setFirmaPass", name = "setFirmaPass")
public class setFirmaPass extends HttpServlet{
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                  getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/jsp/setpassfirma.jsp").forward(request,response);
              
        
}

@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 try{      
     
     
    Funciones objFunciones = new Funciones();
   
      
    String login = (String) request.getSession().getAttribute("login");
    byte[] arraycert = objFunciones.loadCert(login);
     
    
    String password = request.getParameter("ClaveFirma");
     
    KeyStore p12 = KeyStore.getInstance("pkcs12");
    p12.load(new ByteArrayInputStream(arraycert), password.trim().toCharArray());
    Enumeration e = p12.aliases();
    String alias = (String) e.nextElement();
       
    System.out.println("Alias certifikata:" + alias);
    System.out.print("clave ok");
     
   
    objFunciones.setClaveFirma(login, password);
   
    response.sendRedirect("messageview/passfirmaok.html");
     
    }catch(IOException | ClassNotFoundException | KeyStoreException | NoSuchAlgorithmException | CertificateException | SQLException | ParserConfigurationException | SAXException ex){
     System.out.print("error de clave");
       
        response.sendRedirect("messageview/passfirmaerror.html");
    
          
    } 
      
}    
    
    
    
    
    
    
    
    
}
