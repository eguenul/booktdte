package com.egga.bookdte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookdteApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookdteApplication.class, args);
	}

}
